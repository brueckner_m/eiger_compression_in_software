#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	FILE* infile;
	FILE* outfile;

	int read_byte=0;
	unsigned char zero_ctr=0;
	unsigned char prev_byte=1;	// init as not zero
	unsigned char curr_byte=1;
	unsigned char next_byte=1;
	int last_round = 0;

	infile=fopen(argv[1], "rb");
	outfile=fopen(argv[2], "wb");

	read_byte=fgetc(infile);
	if (read_byte < 0)
		return -1;
	next_byte=read_byte;

	while(last_round == 0)
	{
		prev_byte = curr_byte;
		curr_byte = next_byte;
		read_byte = fgetc(infile);
		if (read_byte < 0)
		{
			next_byte = 1;
			last_round = 1;
		}
		else
			next_byte = read_byte;

		// First case, don't do anything beside FF->FE
		if (
			(prev_byte != 0 && curr_byte != 0 && next_byte != 0) ||
			(prev_byte != 0 && curr_byte != 0 && next_byte == 0) ||
			(prev_byte != 0 && curr_byte == 0 && next_byte != 0) ||
			(prev_byte == 0 && curr_byte != 0 && next_byte != 0) ||
			(prev_byte == 0 && curr_byte != 0 && next_byte == 0)
		) 
		{
			if (curr_byte == 0xFF)
				fputc(0xFE, outfile);
			else
				fputc(curr_byte, outfile);
		}
		// Next bytes are zeros, start compression
		else if (prev_byte != 0 && curr_byte == 0 && next_byte == 0)
		{
			fputc(0xFF, outfile);
			zero_ctr=1;
		}
		// Last bytes where zeros, next byte not, stop compression
		else if (prev_byte == 0 && curr_byte == 0 && next_byte != 0)
		{
			fputc(zero_ctr+1, outfile);
		}
		// In the middle of zeros, just increment counter
		else if (prev_byte == 0 && curr_byte == 0 && next_byte == 0)
		{
			if (zero_ctr == 253)
			{
				fputc(0xFF, outfile);
				zero_ctr = zero_ctr +2;
			}
			else
				zero_ctr++;
		}
	}
}
