#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int get_packet(FILE* inputfile,  unsigned char* databytes, int maxsize);
void put_packet(FILE* outputfile, unsigned char* databytes, int size);
