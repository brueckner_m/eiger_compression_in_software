#include "handle_datafiles.h"

//int main(int argc, char* argv[])
int get_packet(FILE* inputfile, unsigned char* databytes, int maxsize)
{
	char* linebuffer = NULL;
	char* past_data_pointer = NULL;
	size_t linelen = 0;
	ssize_t nread;
//	FILE* inputfile;
	union
	{
		unsigned long ulong;
		unsigned char byte[8];
	} data;
	unsigned rem;
	unsigned sofn;
	unsigned eofn;
	unsigned srcrdyn;

//	unsigned char databytes[DATASIZE];
	int in_packet=0;

	int datapos = 0;
	int i;


//	inputfile = fopen(argv[1], "r");

	while (1)
	{
		nread = getline(&linebuffer, &linelen, inputfile);
		if (nread == -1) break;

		if (linebuffer[0] == '\n' || linebuffer[0] == '#')
			continue;

		past_data_pointer = strchr(&linebuffer[1], ' '); // Hack to skip spaces at begin of line
		data.ulong = strtoul(linebuffer, &past_data_pointer, 16);

		rem  = past_data_pointer[1]-0x30;
		sofn = past_data_pointer[3]-0x30;
		eofn = past_data_pointer[5]-0x30;
		srcrdyn = past_data_pointer[7]-0x30;

		if (((in_packet == 1) || (sofn == 0)) && (srcrdyn == 0))
		{
			for (i=0 ; i <= rem; i++)
				databytes[datapos+i] = data.byte[i];
			datapos = datapos + i;

			if (sofn == 0) in_packet = 1;
			if (eofn == 0) break;
		}
	}
	return datapos;

}


void put_packet(FILE* outputfile, unsigned char* databytes, int size)
{
	int i;
	unsigned long* tempdata;
	int rem;

	for (i=0; i < size; i=i+8)
	{
		if (size-i < 8) rem = size-i-1;
		else rem=7;
		tempdata=(unsigned long*) &databytes[i];
		fprintf(outputfile, " %.16lX %d\n", *tempdata, rem);
	}
}
