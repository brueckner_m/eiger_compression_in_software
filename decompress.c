#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	FILE* infile;
	FILE* outfile;

	int read_byte;
	int i;

	infile=fopen(argv[1], "rb");
	outfile=fopen(argv[2], "wb");

	while(1)
	{
		read_byte = fgetc(infile);
		if (read_byte < 0)
			break;

		if (read_byte != 0xFF)
			fputc(read_byte, outfile);
		else
		{
			do
			{
				read_byte = fgetc(infile);
				for(i=0; i < read_byte; i++)
					fputc(0x00, outfile);
			}while (read_byte == 0xFF);

		}
	}
}
